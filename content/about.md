---
title: "About"
date: 2020-02-01T10:15:28+01:00
draft: false
---


# Our mission

Our mission is to build a state of the art business intelligence platform providing best of breed data analytics to airlines.

# Data Aggregation

Aviation data is currently available from a multitude of heterogeneous and somewhat disparate sources, thus making data analysis complicated. Aviation BI's autonomous agents automatically aggregate and consolidate essential aviation data from a variety of online sources and partners, providing the data through a single unified platform.

# Data analytics

Data, albeit essential, is one aspect of the Aviation BI platform. Our goal is also to develop 
innovative analytics to enable airlines to turn data into insights. Using the Aviation BI platform, airlines can:

- Optimize fuel consumption and hedge fuel costs
- Implement flight operations quality assurance processes
- Visualize in realtime flight operations
- Compare airline offerings to those of their competitors
- and much more


# Contributing to protecting the environment

As environmental issues become increasingly important, we also want to actively contribute by developing data analytics enabling airlines to adopt strategies minimizing detrimental emissions to our environment. 